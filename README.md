# L.Chat Backend
to setup run
```
composer install
```
```
sudo docker-compose up -d
```

for development run
```
sudo docker-compose run --rm laravel /bin/bash
# php artisan migrate && php artisan db:seed
```


Thanks [rapidapi](https://rapidapi.com/blog/how-to-create-an-api-in-php/) for the installation guide.
