<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $content = json_decode($request->getContent(), true);
        if (isset($content['username']) && isset($content['password'])) {
            $user = DB::table('chatusers')
                ->where('name', $content['username'])
                ->first();
            if(isset($user) && Hash::check($content['password'], $user->password)) {
                return response('{"userid": "'.$user->id.'"}', 200)
                    ->header('Content-Type', 'text/json');
            }
        }
        return response('{"error": "wrong user credentials"}', 401)
            ->header('Content-Type', 'text/json');
    }
}
