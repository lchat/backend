<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
    public function all(Request $request)
    {
        $user = $this->getUser($request);
        if (isset($user)) {
            if (isset($request['from'])) {
                $messages = DB::table('messages')
                    ->join('chatusers', 'messages.userid', '=', 'chatusers.id')
                    ->where('messages.created_at', '>', $request['from'])
                    ->select('messages.created_at as timestamp',
                        'chatusers.id as userid',
                        'chatusers.name as username',
                        'chatusers.avatar as useravatar',
                        'messages.message')
                    ->get();
                return $this->sendResponse('{"messages":'.$messages.'}', 200);
            }
            return $this->sendResponse('{"error": "no from timestamp set"}', 400);
        }
        return $this->sendResponse('{"error": "missing user credentials"}', 401);
    }

    public function create(Request $request)
    {
        $user = $this->getUser($request);
        if (isset($user)) {
            $content = json_decode($request->getContent(), true);
            if (isset($content['message'])) {
                DB::table('messages')->insert([
                    'userid' => $user->id,
                    'message' => $content['message']
                ]);
                return $this->sendResponse('{"message": "success"}', 200);
            }
            return $this->sendResponse('{"error": "no message set"}', 400);
        }
        return $this->sendResponse('{"error": "missing user credentials"}', 401);
    }

    private function sendResponse($message, $code) {
        return response($message, $code)->header('Content-Type', 'text/json');
    }

    private function getUser($request) {
        $userid = $request->header('X-USERID');
        return $userid != null ? DB::table('chatusers')->where('id', $userid)->first() : null;
    }

    private function formatDate($dateString) {
        return 'DATE';
    }
}
