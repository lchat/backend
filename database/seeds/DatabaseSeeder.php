<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('chatusers')->delete();
        DB::table('messages')->delete();

        DB::table('chatusers')->insert([
            'id' => 'b6e20f3c-a35e-11ea-bb37-0242ac130002',
            'name' => 'Jakob',
            'password' => Hash::make('password'),
            'avatar' => 'photo-1542736637-74169a802172'
        ]);
        DB::table('chatusers')->insert([
            'id' => '128d5c1b-4592-46a6-840d-054d2f8b890f',
            'name' => 'Lukas',
            'password' => Hash::make('password'),
            'avatar' => 'photo-1515390602383-06c70eef50bb'
        ]);
        DB::table('chatusers')->insert([
            'id' => 'ab0c968f-5852-4f43-b42d-6ca4d2d01ff4',
            'name' => 'Marion',
            'password' => Hash::make('password'),
            'avatar' => 'photo-1561037404-61cd46aa615b'
        ]);
        DB::table('chatusers')->insert([
            'id' => 'cf6aa779-7fa9-41ff-a077-9de196734970',
            'name' => 'Susanne',
            'password' => Hash::make('password'),
            'avatar' => 'photo-1536590158209-e9d615d525e4'
        ]);

        DB::table('messages')->insert([
            'userid' => 'b6e20f3c-a35e-11ea-bb37-0242ac130002',
            'message' => 'Hello, nice to meet you. :heart:'
        ]);        
    }
}
